# fairino_python_sdk 

简介
-------------
适用于 Windows 和 Linux 操作系统的 FR 系列协作机器人的 Python SDK。

文档
----------------
请查阅 [Python SDK手册](https://fr-documentation.readthedocs.io/zh_CN/latest/SDKManual/python_intro.html)。